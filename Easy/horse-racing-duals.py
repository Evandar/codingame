pi = []
n = int(raw_input())
for i in xrange(n):
    pi.append(int(raw_input()))

pi.sort()

MIN = 99999999
for i in xrange(1, len(pi)):
    MIN = min(MIN, pi[i] - pi[i-1])

print MIN
