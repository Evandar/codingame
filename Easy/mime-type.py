n = int(raw_input())  # Number of elements which make up the association table.
q = int(raw_input())  # Number Q of file names to be analyzed.
formats = {}
for i in xrange(n):
    shr, mnt = raw_input().split()
    formats[shr.lower()] = mnt


for i in xrange(q):
    fname = raw_input()
    shortcut = fname.split('.')[-1]
    print formats.get(shortcut.lower() if '.' in fname else '', 'UNKNOWN')

