def solve(temps_str):
    temps_list = [int(temp) for temp in temps_str.split(' ')]
    EPS = 0.0001
    MIN = 999999

    for i in temps_list:
        if abs(int(i) - EPS) < abs(MIN):
            MIN = i

    return MIN


n = int(raw_input())
temps_str = raw_input()

print 0 if n == 0 else solve(temps_str)
