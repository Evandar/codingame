import sys

light_x, light_y, initial_tx, initial_ty = [int(i) for i in raw_input().split()]

dir_x = light_x - initial_tx
dir_y = light_y - initial_ty

# game loop
while True:
    remaining_turns = int(raw_input())  # The remaining amount of turns Thor can move. Do not remove this line.

    print >> sys.stderr, dir_x, dir_y

    order = ""

    if dir_y < 0:
        order += "N"
        dir_y += 1
    if dir_y > 0:
        order += "S"
        dir_y -= 1

    if dir_x < 0:
        order += "W"
        dir_x += 1
    if dir_x > 0:
        order += "E"
        dir_x -= 1

    print order

