message = raw_input()
message_bin = ""
answer = ""
last = ""

for letter in message:
    message_bin += format(ord(letter), '07b')

for c in message_bin:
    if c != last:
        answer = answer + ' '
        answer += '0 ' if c == '1' else '00 '
        last = c
    answer += '0'

print answer[1:]

