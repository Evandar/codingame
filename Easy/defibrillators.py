import math

def make_float(x):
    return float(x.replace(',', '.'))

def distance(ax, ay, bx, by):
    return math.sqrt(pow((ax - bx),2) + pow((ay - by), 2))

lon = make_float(raw_input())
lat = make_float(raw_input())
n = int(raw_input())

defib = []
for i in xrange(n):
    defib.append(raw_input().split(';'))

val, idx = min((distance(lon, lat, make_float(v[4]), make_float(v[5])), id) for (id, v) in enumerate(defib))
print defib[idx][1]
