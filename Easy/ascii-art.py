import sys

l = int(raw_input())
h = int(raw_input())
t = raw_input()

alphabet = [[] for i in xrange(h)]
answer = []

for i in xrange(h):
    alphabet[i] = raw_input()

for c in t:

    if c.isupper():
        c_base = ord(c) - ord('A')
    elif c.islower():
        c_base = ord(c) - ord('a')
    else:
        c_base = 26
    answer.append((c_base * l, (c_base + 1)*l))

for i in xrange(h):
    for tup in answer:
        sys.stdout.write(alphabet[i][tup[0]:tup[1]])
    sys.stdout.write('\n')
