from itertools import permutations

dictionary = dict()
letters = {
    'e': 1, 'a': 1, 'i': 1, 'o': 1, 'n': 1, 'r': 1, 't': 1, 'l': 1, 's': 1, 'u': 1,
    'd': 2, 'g': 2,
    'b': 3, 'c': 3, 'm': 3, 'p': 3,
    'f': 4, 'h': 4, 'v': 4, 'w': 4, 'y': 4,
    'k': 5,
    'j': 8, 'x': 8,
    'q': 10, 'z': 10
}

n = int(raw_input())
for i in xrange(n):
    word = raw_input()
    dictionary[word] = (sum([letters[j] for j in word]), i)

letters = raw_input()

result = {(dictionary.get(''.join(p), 0), ''.join(p)) for i in xrange(1, len(letters)+1) for p in permutations(letters, i) if dictionary.get(''.join(p), 0) > 0}
MAX = max(result)[0][0]
print min([(i[0][1], i[1]) for i in result if i[0][0] == MAX])[1]
