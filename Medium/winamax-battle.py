import sys
import math

# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.


turn = 0


def parse_card(card):
    card = card[:-1]
    card_map = {
        'A': 14,
        'K': 13,
        'Q': 12,
        'J': 11,
    }

    if card in card_map:
        return card_map[card]
    else:
        return int(card)


def fight():
    game_p1.append(deck_p1.pop(0))
    game_p2.append(deck_p2.pop(0))

    print >> sys.stderr, "Fight", game_p1, game_p2
    if game_p1[-1] > game_p2[-1]:
        # print >> sys.stderr, "Wygyrywa 1"
        deck_p1.extend(game_p1[:])
        deck_p1.extend(game_p2[:])
        del game_p1[:]
        del game_p2[:]

    elif game_p1[-1] < game_p2[-1]:
        # print >> sys.stderr, "Wygyrywa 2"
        deck_p2.extend(game_p1[:])
        deck_p2.extend(game_p2[:])
        del game_p2[:]
        del game_p1[:]

    else:
        war()


def war():
    print >> sys.stderr, "War"
    if len(deck_p1) <= 3 or len(deck_p2) <= 3:
        print "PAT"

    for i in xrange(3):
        game_p1.append(deck_p1.pop(0))

    for i in xrange(3):
        game_p2.append(deck_p2.pop(0))

    fight()


n = int(raw_input())  # the number of cards for player 1
deck_p1 = [parse_card(raw_input()) for i in xrange(n)]

m = int(raw_input())  # the number of cards for player 2
deck_p2 = [parse_card(raw_input()) for i in xrange(m)]

game_p1 = []
game_p2 = []

while True:
    # print >> sys.stderr, deck_p1, deck_p2

    if not deck_p1:
        print 2, turn
        break

    if not deck_p2:
        print 1, turn
        break

    fight()

    turn += 1

