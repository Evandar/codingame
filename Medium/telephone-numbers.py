numbers = set()

n = int(raw_input())
for i in xrange(n):
    telephone = raw_input()
    numbers |= {telephone[:j+1] for j in range(len(telephone))}

print len(numbers)