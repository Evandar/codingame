budgets = list()
result = list()

n = int(raw_input())
c = int(raw_input())

for i in xrange(n):
    budgets.append(int(raw_input()))

budgets.sort()

for i in budgets:
    if i < (c / n):
        result.append(i)
        c -= i
    else:
        result.append(c/n)
        c -= (c/n)
    n -= 1

if not c:
    for i in result:
        print i
else:
    print "IMPOSSIBLE"