def DFS(v, distance):
    for to in graph[v]:
        if to not in distance:
            distance[to] = distance[v] + 1
            DFS(to, distance)

    return distance


graph = dict()
indegree = dict()
n = int(raw_input())  # the number of relationships of influence
for i in xrange(n):
    x, y = [int(j) for j in raw_input().split()]
    graph.setdefault(x, []).append(y)
    graph.setdefault(y, [])
    indegree[y] = indegree.get(y, 0) + 1

to_check = {i: 0 for i in graph.keys() if i not in indegree.keys()}

MAX = 0
for key in to_check:
    dist = DFS(key, {key: 1})
    MAX = max(max([(value, key) for key, value in dist.iteritems()]), MAX)

print MAX[0]