raw_input()

result = max_i = 0

for i in raw_input().split():
    data_i = int(i)
    max_i = max(max_i, data_i)
    result = min(result, data_i - max_i)

print result