nb_floors, width, nb_rounds, exit_floor, exit_pos, nb_total_clones, nb_additional_elevators, nb_elevators = [int(i) for i in raw_input().split()]

elevator = dict()
elevator[exit_floor] = exit_pos

for i in xrange(nb_elevators):
    elevator_floor, elevator_pos = [int(j) for j in raw_input().split()]
    elevator[elevator_floor] = elevator_pos

while True:
    clone_floor, clone_pos, direction = raw_input().split()
    clone_floor = int(clone_floor)
    clone_pos = int(clone_pos)

    if clone_pos == -1:
        print 'WAIT'
        continue

    elevator_diff = elevator[clone_floor] - clone_pos

    if (elevator_diff < 0 and direction == 'RIGHT')\
            or (elevator_diff > 0 and direction == 'LEFT'):
        _next = 'BLOCK'
    else:
        _next = 'WAIT'

    print _next