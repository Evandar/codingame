r = int(raw_input())
l = int(raw_input())

conway = [r]

for i in xrange(l-1):
    new_conway = []
    acc_i = 0
    acc_val = -1
    for j in conway:
        if j == acc_val:
            acc_i += 1
        else:
            if acc_i:
                new_conway += [acc_i, acc_val]
            acc_val, acc_i = j, 1

    new_conway += [acc_i, acc_val]
    conway = new_conway

for i in conway:
    print i,