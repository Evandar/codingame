def BFS(start_n):
    prev = {}
    dist = {}

    q = [start_n]
    prev[start_n] = -1
    dist[start_n] = 0

    while q:
        v = q.pop(0)

        for to in (temp for temp in graph[v] if temp not in dist):
            prev[to] = v
            dist[to] = dist[v] + 1
            q += [to]

    return prev, dist

graph = {}
n, l, e = [int(i) for i in raw_input().split()]

for i in xrange(n):
    graph[i] = []

for i in xrange(l):
    n1, n2 = [int(j) for j in raw_input().split()]
    graph[n1].append(n2)
    graph[n2].append(n1)

gateways = [int(raw_input()) for i in xrange(e)]

while True:
    si = int(raw_input())  # The index of the node on which the Skynet agent is positioned this turn

    prev, dist = BFS(si)

    cut_id = min((value, key) for key, value in dist.iteritems() if key in gateways)

    graph[prev[cut_id[1]]].remove(cut_id[1])
    graph[cut_id[1]].remove(prev[cut_id[1]])

    print prev[cut_id[1]], cut_id[1]
