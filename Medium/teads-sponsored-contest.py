def BFS(start_node):
    q = list()
    distance = dict()

    distance[start_node] = 0
    q.append(start_node)

    while q:
        v = q.pop(0)
        for to in graph[v]:
            if to not in distance:
                distance[to] = distance[v] + 1
                q.append(to)

    return distance


def tree_center():
    d = {key: len(value) for key, value in graph.iteritems()}
    q = [key for key, value in graph.iteritems() if len(value) == 1]

    while len(q) > 1:
        v = q.pop(0)
        d[v] -= 1
        for to in graph[v]:
            if d[to] > 0:
                d[to] -= 1
                if d[to] == 1:
                    q.append(to)

    return q[0]


n = int(raw_input())

graph = {}

for i in xrange(n):
    xi, yi = [int(j) for j in raw_input().split()]
    graph.setdefault(xi, []).append(yi)
    graph.setdefault(yi, []).append(xi)

distance = BFS(tree_center())
print max([i for i in distance.values()])
