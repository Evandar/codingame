block = dict()
block[(2, 'LEFT')] = (1, 0)
block[(2, 'RIGHT')] = (-1, 0)
block[(4, 'TOP')] = (-1, 0)
block[(5, 'TOP')] = (1, 0)
block[(6, 'LEFT')] = (1, 0)
block[(6, 'RIGHT')] = (-1, 0)
block[(10, 'TOP')] = (-1, 0)
block[(11, 'TOP')] = (1, 0)

w, h = [int(i) for i in raw_input().split()]

maze = [[] for i in xrange(h)]

for i in xrange(h):
    maze[i] = [int(j) for j in raw_input().split()]  # represents a line in the grid and contains W integers. Each integer represents one room of a given type.
ex = int(raw_input())  # the coordinate along the X axis of the exit (not useful for this first mission, but must be read).


while True:
    xi, yi, pos = raw_input().split()
    xi, yi = int(xi), int(yi)

    change = block.get((maze[yi][xi], pos), (0, 1))

    xi += change[0]
    yi += change[1]

    print xi, yi