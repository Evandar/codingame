import sys

# w: width of the building.
# h: height of the building.
w, h = [int(i) for i in raw_input().split()]
n = int(raw_input())  # maximum number of turns before game over.
x0, y0 = [int(i) for i in raw_input().split()]

# game loop

min_x, max_x = 0, w - 1
min_y, max_y = 0, h - 1

while True:
    bomb_dir = raw_input()  # the direction of the bombs from batman's current location (U, UR, R, DR, D, DL, L or UL)
    print >> sys.stderr, bomb_dir

    if 'D' in bomb_dir:
        min_y = y0 + 1

    if 'U' in bomb_dir:
        max_y = y0 - 1

    if 'R' in bomb_dir:
        min_x = x0 + 1

    if 'L' in bomb_dir:
        max_x = x0 - 1

    x0 = (max_x + min_x) / 2
    y0 = (max_y + min_y) / 2

    print x0, y0
