houses = []
n = int(raw_input())
for i in xrange(n):
    x, y = [int(j) for j in raw_input().split()]
    houses.append((y, x))

min_x = min([i[1] for i in houses])
max_x = max([i[1] for i in houses])

houses.sort()

y_cable = houses[n/2][0]
additional_cable = sum([abs(i[0] - y_cable) for i in houses])

print max_x-min_x + additional_cable