mayan_to_dec = {}
dec_to_mayan = {}


def convert_to_dec(mayan_digits):
    power = 0
    sum = 0

    for i in reversed(mayan_digits):
        sum += mayan_to_dec[i] * pow(20, power)
        power += 1

    return str(sum)


def convert_to_mayan(dec_number):
    result = []
    if dec_number == 0:
        print dec_to_mayan[0]
        return

    while dec_number:
        result.append(dec_number % 20)
        dec_number /= 20

    for i in result[::-1]:
        print dec_to_mayan[i]


l, h = [int(i) for i in raw_input().split()]

digits = [raw_input() for i in xrange(h)]

for i in xrange(20):
    digit_key = '\n'.join([digits[j][i*l: (i+1) * l] for j in xrange(h)])
    mayan_to_dec[digit_key] = i
    dec_to_mayan[i] = digit_key


s1 = int(raw_input())
first_number = ['\n'.join([raw_input() for j in xrange(l)]) for i in xrange(s1/h)]

s2 = int(raw_input())
second_number = ['\n'.join([raw_input() for j in xrange(l)]) for i in xrange(s2/h)]

operation = raw_input()

final_result = eval(' '.join([convert_to_dec(first_number), operation, convert_to_dec(second_number)]))

convert_to_mayan(final_result)


