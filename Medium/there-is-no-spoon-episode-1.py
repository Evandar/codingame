class Node():
    def __init__(self, status="blank"):
        self.status = status
        self.right = (-1,-1)
        self.bottom = (-1,-1)

    def __str__(self):
        return "%s || %s" % (self.right, self.bottom)


width = int(raw_input())  # the number of cells on the X axis
height = int(raw_input())  # the number of cells on the Y axis

field = [[] for i in xrange(height)]
line_lastN = (-1, -1)
vert_lastN = [(-1, -1) for i in xrange(width)]

for i in xrange(height):
    line = raw_input()  # width characters, each either 0 or .

    for j, value in enumerate(line):
        if value == '0':
            field[i].append(Node("node"))
            if line_lastN[0] == i:
                field[line_lastN[0]][line_lastN[1]].right = (i, j)

            if vert_lastN[j] != (-1,-1):
                field[vert_lastN[j][0]][vert_lastN[j][1]].bottom = (i, j)

            line_lastN = (i, j)
            vert_lastN[j] = (i, j)

        else:
            field[i].append(Node())

for i, i_val in enumerate(field):
    for j, j_val in enumerate(i_val):
        if field[i][j].status == 'node':
            print j, i, field[i][j].right[1], field[i][j].right[0], field[i][j].bottom[1], field[i][j].bottom[0]

